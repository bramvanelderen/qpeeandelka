﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Porcupine
{
	public class ElkController : MonoBehaviour
	{
	    [SerializeField] private Range _insultInterval;
	    [SerializeField] private CustomClip _speech;
	    [SerializeField] private Range _speechInterval;

	    private List<TextBoxInfo> _textBoxes;
	    private bool _isActive = false;
	    private float _untilNextInsult;

	    private List<TextBoxInfo> _activeBoxes = new List<TextBoxInfo>();
	    private AudioSource _audio;
	    private float _untilNextSpeech = 0;

	    private ElkTexts _texts;

		void Start ()
		{
		    _texts = Resources.Load<ElkTexts>("ElkTexts");
		    _audio = GetComponent<AudioSource>();
		    _audio.playOnAwake = false;
		    _audio.spatialBlend = .7f;
		    _textBoxes = new List<TextBoxInfo>();
		    for (int i = 0; i < transform.childCount; i++)
		    {
		        var box = transform.GetChild(i).gameObject;
		        _textBoxes.Add(new TextBoxInfo(box));
		    }
		}
		
		void Update () 
		{
		    if (_isActive && Time.time > _untilNextInsult)
		    {
		        DisplayText(_texts._randomInsults[Random.Range(0, _texts._randomInsults.Count)]);
		        _untilNextInsult = _insultInterval.Random + Time.time;
		    }

		    if (_activeBoxes.Count > 0 && Time.time > _untilNextSpeech)
		    {
		        _untilNextSpeech = Time.time + _speechInterval.Random;
		        _speech.Play(_audio);
		    }
		}


	    public void Initialize()
	    {
	        StartCoroutine(Intro());
	        _untilNextInsult = Time.time + 12f;
	        _isActive = true;
	    }

	    public void Stop()
	    {
	        _isActive = false;
            StopAllCoroutines();

	        foreach (var box in _textBoxes)
	        {
                box.HideText();
                _activeBoxes.Remove(box);
                _untilNextInsult = 0f;
                _untilNextSpeech = 0f;
            }
        }

	    IEnumerator Intro()
	    {
	        foreach (var text in _texts._introTexts)
	        {
	            DisplayText(text);
	            yield return new WaitForSeconds(text.Duration - .5f);
	        }
	    }

	    void DisplayText(SpokenText text)
	    {
	        List<TextBoxInfo> _availableBoxes = new List<TextBoxInfo>();
	        foreach (var item in _textBoxes)
	        {
	            if (!item.IsInUse)
	                _availableBoxes.Add(item);
	        }

	        if (_availableBoxes.Count == 0)
	            return;

	        var box = _availableBoxes[Random.Range(0, _availableBoxes.Count)];
            box.DisplayText(text.Content);
	        _activeBoxes.Add(box);
            StartCoroutine(HideText(box, text.Duration));
	    }

	    IEnumerator HideText(TextBoxInfo box, float duration)
	    {
	        yield return new WaitForSeconds(duration);
            _activeBoxes.Remove(box);
	        box.HideText();
	    }



	    



	    class TextBoxInfo
	    {
	        public GameObject TextBox;
	        public bool IsInUse = false;
	        private List<GameObject> _components;
	        private TMPro.TextMeshPro _text;

	        public TextBoxInfo(GameObject box)
	        {
	            TextBox = box;
	            _components = new List<GameObject>();
	            for (int i = 0; i < TextBox.transform.childCount; i++)
	            {
	                var child = TextBox.transform.GetChild(i);
	                var text = child.GetComponent<TMPro.TextMeshPro>();
	                if (text)
	                    _text = text;

	                _components.Add(child.gameObject);
	                child.gameObject.SetActive(false);
	            }
	        }

	        public void DisplayText(string text)
	        {
	            foreach (var comp in _components)
	            {
	                comp.SetActive(true);
	            }
	            _text.text = text;
	            IsInUse = true;
	        }

	        public void HideText()
	        {
                foreach (var comp in _components)
                {
                    comp.SetActive(false);
                }
	            IsInUse = false;
	        }
	    }
	}
}

