﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Porcupine
{
	public class ObjectSpawner : MonoBehaviour
	{
	    [SerializeField] private List<GameObject> _prefabs;
	    [SerializeField] private float _spawnHeight;
        [SerializeField] private TransformRange _spawnArea;
	    [SerializeField] private List<TransformRange> _excludedAreas;
	    [SerializeField] private FloatRange _Interval;
	    [SerializeField] public bool IsSpawning = true;

	    private List<ObjectPool> _pools;

	    void Start()
	    {
	        _pools = new List<ObjectPool>();
	        foreach (var prefab in _prefabs)
	            AddPrefab(prefab, 100);

	        StartCoroutine(Spawn());
	    }

        /// <summary>
        /// The spawn routine spawns an object every interval based on a random value within a range
        /// </summary>
        /// <returns></returns>
	    IEnumerator Spawn()
	    {
	        yield return new WaitForSeconds(_Interval.RandomFloat);
	        if (IsSpawning)
	        {
	            //Spawn object
	            var obj = _pools[Random.Range(0, _pools.Count)].GetObject();
	            var position = new Vector3(_spawnArea.RandomX, _spawnHeight, 0);

                bool isExcluded = false;
                foreach (var area in _excludedAreas)
                {
                    if (position.x > area.Start.position.x && position.x < area.End.position.x)
                        isExcluded = true;
                }

	            if (!isExcluded)
	            {
	                obj.transform.position = position;
	            }
	            else
	            {
	                obj.SetActive(false);
	            }
	        }

	        StartCoroutine(Spawn());
	    }

	    public void AddExcludedArea(Transform start, Transform end)
	    {
	        var range = new TransformRange();
	        range.Start = start;
	        range.End = end;
	        _excludedAreas.Add(range);
	    }

	    public void AddPrefab(GameObject prefab, int poolSize)
	    {
	        if (_pools.Any(x => x.ComparePrefab(prefab)))
	            return;

	        var pool = new ObjectPool(prefab, poolSize, true);
	        _pools.Add(pool);
	    }

	    public void CleanUp()
	    {
	        foreach (var pool in _pools)
	        {
	            pool.CleanUp();
	        }
	    }

        [System.Serializable]
	    public struct TransformRange
	    {
	        public Transform Start;
	        public Transform End;

	        public float RandomX
	        {
	            get
	            {
	                return Random.Range(Start.position.x, End.position.x);
	            }
	        }
	    }

        [System.Serializable]
	    public struct FloatRange
	    {
	        public float Start;
	        public float End;

	        public FloatRange(float start, float end)
	        {
	            Start = start;
	            End = end;
	        }

	        public float RandomFloat
	        {
	            get { return Random.Range(Start, End); }
	        }
	    }
	}
}

