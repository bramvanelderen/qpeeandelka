﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Porcupine
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class BrickController : MonoBehaviour, IHit
    {
        [SerializeField] private Sprite _buildSprite;
        [SerializeField] private Sprite _finishedSprite;

        [SerializeField] private CustomClip _pop;

        private AudioSource _audio;
        private SpriteRenderer _renderer;

        void Start()
        {
            gameObject.AddAudioSource(_audio);
            _renderer = GetComponent<SpriteRenderer>();
            _renderer.sprite = _buildSprite;
        }

        void Update()
        {

        }

        public static BrickController Initialize(GameObject obj, bool active)
        {
            var brick = obj.AddComponent<BrickController>();
            obj.SetActive(active);


            return brick;
        }

        public static BrickController Initialize(Transform obj, bool active)
        {

            return Initialize(obj.gameObject, active);
        }

        public void Finish()
        {
            _renderer.sprite = _finishedSprite;
            Destroy(GetComponent<Collider2D>());
        }

        public void Hit()
        {
            
        }

        public void Kill()
        {
            //var audioObj = new GameObject();
            //_pop.Play(audioObj.AddAudioSource());
            //Destroy(audioObj, 2f);

            //gameObject.SetActive(false);
            //play destroy fx
        }
    }
}

