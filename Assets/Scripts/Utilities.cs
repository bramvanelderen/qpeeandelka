﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace Porcupine
{
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;

        private static object _lock = new object();

        public Action DestroyCallback;

        public static T Instance
        {
            get
            {
                if (applicationIsQuitting)
                {
                    Debug.LogWarning("[Singleton] Instance '" + typeof(T) +
                        "' already destroyed on application quit." +
                        " Won't create again - returning null.");
                    return null;
                }

                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = (T)FindObjectOfType(typeof(T));

                        if (FindObjectsOfType(typeof(T)).Length > 1)
                        {
                            Debug.LogError("[Singleton] Something went really wrong " +
                                " - there should never be more than 1 singleton!" +
                                " Reopening the scene might fix it.");
                            return _instance;
                        }

                        //if (_instance == null)
                        //{
                        //    GameObject singleton = new GameObject();
                        //    _instance = singleton.AddComponent<T>();
                        //    singleton.name = "(singleton) " + typeof(T).ToString();

                        //    DontDestroyOnLoad(singleton);

                        //    Debug.Log("[Singleton] An instance of " + typeof(T) +
                        //        " is needed in the scene, so '" + singleton +
                        //        "' was created with DontDestroyOnLoad.");
                        //}
                        //else {
                        //    Debug.Log("[Singleton] Using instance already created: " +
                        //        _instance.gameObject.name);
                        //}
                    }

                    return _instance;
                }
            }
        }

        private static bool applicationIsQuitting = false;
        /// <summary>
        /// When Unity quits, it destroys objects in a random order.
        /// In principle, a Singleton is only destroyed when application quits.
        /// If any script calls Instance after it have been destroyed, 
        ///   it will create a buggy ghost object that will stay on the Editor scene
        ///   even after stopping playing the Application. Really bad!
        /// So, this was made to be sure we're not creating that buggy ghost object.
        /// </summary>
        public void OnDestroy()
        {
            //applicationIsQuitting = true;
            if (DestroyCallback != null)
                DestroyCallback();
        }
    }

    /// <summary>
    /// An audio wrapper which makes it easy to play clips with a variety of custom settings
    /// </summary>
    [System.Serializable]
    public class CustomClip
    {
        [Header("Clip Settings")]
        public List<AudioClip> Clips = new List<AudioClip> { null };
        public float Volume = 1f;
        public Range Pitch = new Range() { Min = 1f, Max = 1f };
        public Range LowPass = new Range() { Min = 1f, Max = 1f };
        public float Reverb = 1f;
        public bool Loop = false;
        public AudioRolloffMode Rolloff = AudioRolloffMode.Logarithmic;
        public AudioMixerGroup Group;

        public AudioClip Clip
        {
            get
            {
                if (Clips == null || Clips.Count == 0)
                    return null;

                return Clips[UnityEngine.Random.Range(0, Clips.Count)];
            }
        }

        /// <summary>
        /// Play an audio clip with all the custom settings
        /// </summary>
        /// <param name="audio"></param>
        public void Play(AudioSource audio)
        {
            if (Clip == null)
                return;

            //Apply filters
            var lowPass = audio.gameObject.GetComponent<AudioLowPassFilter>();
            if (!lowPass)
                lowPass = audio.gameObject.AddComponent<AudioLowPassFilter>();
            lowPass.cutoffFrequency = 22000f * LowPass.Random;

            //Apply Audio source settings and play
            audio.Stop();
            audio.clip = Clip;
            audio.volume = Volume;
            audio.pitch = Pitch.Random;
            audio.loop = Loop;
            audio.reverbZoneMix = Reverb;
            audio.rolloffMode = Rolloff;
            audio.outputAudioMixerGroup = Group;
            audio.Play();
        }

        public void Play(MonoBehaviour component)
        {
            var audio = component.GetComponent<AudioSource>();
            if (!audio)
                audio = component.gameObject.AddComponent<AudioSource>();

            Play(audio);
        }
    }

    /// <summary>
    /// Range class
    /// </summary>
    [System.Serializable]
    public class Range
    {
        public float Min;
        public float Max;

        /// <summary>
        /// Returns a random value within the min max range
        /// </summary>
        public float Random
        {
            get
            {
                return UnityEngine.Random.Range(Min, Max);
            }
        }
    }

    public static class Extensions
    {

        /// <summary>
        /// Retrieves a quaternion rotation in the direction of a given point
        /// </summary>
        /// <param name="transform"></param>
        /// <param name="lookAt"></param>
        /// <returns></returns>
        public static Quaternion LookRotation(this Transform transform, Vector3 lookAt)
        {
            var dir = (lookAt - transform.position).normalized;

            return Quaternion.LookRotation(dir);
        }

        /// <summary>
        /// Retrieves a quaternion rotation in the direction of a given point
        /// </summary>
        /// <param name="transform"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public static Quaternion LookRotation(this Transform transform, float x, float y, float z)
        {
            var lookAt = new Vector3(x, y, z);
            var dir = (lookAt - transform.position).normalized;

            return Quaternion.LookRotation(dir);
        }

        /// <summary>
        /// TODO DONT USE YET IT IS NOT CORRECT YET
        /// </summary>
        /// <param name="quat0"></param>
        /// <param name="quat1"></param>
        /// <returns></returns>
        public static Quaternion Multiply(this Quaternion quat0, Quaternion quat1)
        {
            long w0 = (long)quat0.w;
            var v0 = new Vector3(quat0.x, quat0.y, quat0.z);

            long w1 = (long)quat1.w;
            var v1 = new Vector3(quat1.x, quat1.y, quat1.z);


            long w2 = (long)(w1 * w0 - Vector3.Dot(v1, v0));
            var v2 = w0 * v1 + Vector3.Cross(v1, v0);
            var result = new Quaternion(v2.x, v2.y, v2.z, w2);

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transform"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static Vector3 PositionXZ(this Transform transform, Vector3 target)
        {
            var pos = new Vector3(transform.position.x, target.y, transform.position.z);

            return pos;
        }

        /// <summary>
        /// Orbits a certain position around a given center position and axis by a given angle
        /// </summary>
        /// <param name="position"></param>
        /// <param name="center"></param>
        /// <param name="axis"></param>
        /// <param name="angle"></param>
        /// <returns></returns>
        public static Vector3 RotateAround(this Vector3 position, Vector3 center, Vector3 axis, float angle)
        {
            var result = Vector3.zero;
            var rot = Quaternion.AngleAxis(angle, axis); // get the desired rotation
            var dir = position - center; // find current direction relative to center
            dir = rot * dir; // rotate the direction
            result = center + dir; // define new position

            return result;
        }

        /// <summary>
        /// Returns the direction towards the target
        /// </summary>
        /// <param name="current"></param>
        /// <param name="lookAt"></param>
        /// <param name=""></param>
        /// <returns></returns>
        public static Vector3 Direction(this Transform current, Transform lookAt, bool ignoreVerticalDifference = false)
        {
            var result = current.forward;

            if (ignoreVerticalDifference)
            {
                var currentPos = current.position;
                var targetPos = new Vector3(lookAt.position.x, current.position.y, lookAt.position.z);
                result = (targetPos - currentPos).normalized;
            }
            else
            {
                result = (lookAt.position - current.position).normalized;
            }

            return result;
        }

        /// <summary>
        /// Adds an audiosource to a new gameobject that is a child to the given gameobject
        /// </summary>
        /// <param name="component"></param>
        /// <param name="playOnWake"></param>
        /// <returns></returns>
        public static AudioSource AddAudioSource(this GameObject component, bool playOnWake = false)
        {
            var obj = new GameObject();
            obj.transform.parent = component.transform;
            obj.transform.localPosition = Vector3.zero;
            obj.name = "AudioObject";
            var audio = obj.AddComponent<AudioSource>();
            audio.playOnAwake = playOnWake;

            return audio;
        }

        /// <summary>
        /// Checks if an object of type already exists, 
        /// if so that depending on destroy value it will destroy the existing object
        /// </summary>
        /// <param name="comp"></param>
        /// <param name="destroy"></param>
        /// <returns></returns>
        public static bool CheckIfAlreadyExists(this Component comp, bool destroy = false)
        {
            var result = false;
            foreach (var item in GameObject.FindObjectsOfType(comp.GetType()))
            {
                if (item != comp)
                    result = true;
            }

            if (destroy && result)
                Component.Destroy(comp.gameObject);

            return result;
        }
    }
}
