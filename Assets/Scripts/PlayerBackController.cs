﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Porcupine
{
    public class PlayerBackController : MonoBehaviour
    {
        public void OnCollisionEnter2D(Collision2D collision)
        {
            var comp = collision.gameObject.GetComponent<IHit>();
            if (comp != null)
                comp.Kill();
        }
    }
}

