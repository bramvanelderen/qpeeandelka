﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Porcupine
{
    public class PlayerFrontController : MonoBehaviour
    {
        [SerializeField]
        private CustomClip _collisionSound;
        [SerializeField]
        private float velocityThresholdBump;
        [SerializeField]
        private Range cooldown;

        private AudioSource _audio;

        private float _lastTime = 0f;


        void Start()
        {
            _audio = gameObject.AddAudioSource();
        }

        public void OnCollisionEnter2D(Collision2D collision)
        {
            var hit = collision.gameObject.GetComponent<IHit>();
            if (hit != null && collision.relativeVelocity.magnitude > velocityThresholdBump && Time.time > _lastTime + cooldown.RandomRange)
            {
                    hit.Hit();
                _lastTime = Time.time;                
            }
        }

        [System.Serializable]
        struct Range
        {
            public float Min;
            public float Max;

            public float RandomRange
            {
                get { return Random.Range(Min, Max); }
            }
        }
    }
}

