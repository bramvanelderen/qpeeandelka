﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Porcupine
{
    [CreateAssetMenu(fileName = "ElkTexts", menuName = "Data/ElkTexts")]

    public class ElkTexts : ScriptableObject
    {
        public List<SpokenText> _introTexts;
        public List<SpokenText> _randomInsults;
    }

    [System.Serializable]
    public class SpokenText
    {
        [TextArea]
        public string Content;
        public float Duration;
    }
}
