﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Porcupine
{
    public class BalloonController : MonoBehaviour, IHit
    {
        [SerializeField] private CustomClip _pop;
        [SerializeField]private CustomClip _hit;
        [SerializeField] private ParticleSystem _popParticle;
        private AudioSource _audio;

        public void Start()
        {
            _audio = gameObject.AddAudioSource();
        }

        public void Hit()
        {
            var audioObj = new GameObject();
            _hit.Play(audioObj.AddAudioSource());
            Destroy(audioObj, 2f);
        }

        public void OnEnable()
        {
            
        }

        public void OnDisable()
        {
        }

        void Update()
        {

        }

        public void Kill()
        {
            var audioObj = new GameObject();
            _pop.Play(audioObj.AddAudioSource());
            Destroy(audioObj, 2f);
            var obj = Instantiate(_popParticle);
            obj.transform.position = transform.position;
            Destroy(obj.gameObject, 2f);
            gameObject.SetActive(false);
        }
    }
}

