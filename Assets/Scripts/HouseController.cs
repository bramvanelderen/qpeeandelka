﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

namespace Porcupine
{
    public class HouseController : MonoBehaviour
    {
        [SerializeField] private float _rowHeight = 2f;
        [SerializeField] private CustomClip _score;
        [SerializeField] private ParticleSystem _scoreParticlesPrefab;
        private List<Transform> _rows;
        private bool _isReady = false;
        public bool IsFinished { get; private set; }
        public Action Finish { private get; set; }
        private int[] index;
        private BoxCollider2D _collider;

        private AudioSource _audio;

        void Start()
        {
            _audio = gameObject.AddComponent<AudioSource>();
            _audio.playOnAwake = false;
            Initialize();
        }

        void Update()
        {

        }

        void Initialize()
        {
            IsFinished = false;
            _rows = new List<Transform>();
            for (int i = 0; i < transform.childCount; i++)
            {
                var child = transform.GetChild(i);

                for (int j = 0; j < child.childCount; j++)
                {
                    var subchild = child.GetChild(j);
                    subchild.gameObject.SetActive(false);
                }
                _rows.Add(child);
            }
            _isReady = true;
            index = new int[2];
            index[0] = 0;
            index[1] = 0;
            _collider = GetComponent<BoxCollider2D>();
            _collider.offset = new Vector2(_collider.offset.x, 1 + _rowHeight);
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            var balloon = collision.gameObject.GetComponent<BalloonController>();

            if (balloon)
            {
                Score(1);
                //do stuff with balloon
                balloon.gameObject.SetActive(false);

            }
        }


        public void Score(int scoreCount)
        {
            for (int i = 0; i < scoreCount; i++)
            {
                Score();
            }
        }

        /// <summary>
        /// Checks if any of the building blocks of the house are not enabled yet
        /// if nothing is disabled then it means the game is done
        /// else enable brick and play some effects
        /// </summary>
        public void Score()
        {           

            var row = _rows[index[0]];
            
            Transform result = row.GetChild(index[1]);
            result.gameObject.SetActive(true);

            ///TODO Play particles and sound here
            _score.Play(_audio);
            var obj = Instantiate(_scoreParticlesPrefab, result.position, result.rotation);
            Destroy(obj.gameObject, 2f);

            index[1]++;

            if (row.childCount - 1 < index[1])
            {

                StartCoroutine(FinishRow(index[0]));
                index[0]++;

                if (index[0] > _rows.Count - 1)
                {
                    IsFinished = true;
                    if (Finish != null)
                        Finish();
                    return;
                }

                index[1] = 0;

                _collider.offset = new Vector2(_collider.offset.x, 1 + ((index[0] + 1) * _rowHeight));
            }
        }

        IEnumerator FinishRow(int index)
        {
            yield return null;

            var row = _rows[index];
            for (int i = 0; i < row.childCount; i++)
            {
                row.GetChild(i).gameObject.GetComponent<BrickController>().Finish();
            }
        }

    }
}

