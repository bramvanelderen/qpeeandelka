﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Porcupine
{
    [CustomEditor(typeof(Main))]
	public class MainEditor : Editor 
	{
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            var script = (Main) target;

            if (GUILayout.Button("Finish game"))
            {
                script.FinishGame();
            }

            if (GUILayout.Button("start game"))
            {
                script.StartGame();
            }

        }
    }
}

