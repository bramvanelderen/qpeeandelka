﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Porcupine
{
	public class Main : MonoBehaviour
	{
	    [SerializeField] private GameObject _playerPrefab;
	    [SerializeField] private GameObject _statuePrefab;
	    [SerializeField] private Transform _spawnStatue;
        [SerializeField]private Transform _spawn;

        [SerializeField] private RectTransform _menu;
        [SerializeField] private CameraFollow _follow;
	    [SerializeField] private ElkController _elk;

        private ObjectSpawner _spawner;
	    private GameObject _player;
	    private HouseController _house;

	    private bool _inGame = false;

		void Start ()
		{
		    _spawner = GetComponent<ObjectSpawner>();
		    _spawner.IsSpawning = false;
            GameObject.Find("StartButton").GetComponent<Button>().onClick.AddListener(() => StartGame());
            GameObject.Find("ExitButton").GetComponent<Button>().onClick.AddListener(() => ExitGame());


            _player = Instantiate(_playerPrefab);
            _player.transform.position = _spawn.position;
		    EventScript.Instance.SetSelectedGameObject(GameObject.Find("StartButton"));
		}

	    void Update()
	    {
	        if (_inGame && InputWrapper.GetButtonDown(InputButtons.Start))
	            FinishGame();

	    }

	    public void StartGame()
	    {
	        if (_inGame)
	            return;

	        if (_house)
	            Destroy(_house.gameObject);


            _menu.gameObject.SetActive(false);
	        _player.GetComponent<PlayerMovement>().IsInControl = true;
	        _follow.Target = _player.transform;
	        _follow.TargetTransform = true;
	        _spawner.IsSpawning = true;

	        var statue = Instantiate(_statuePrefab);
	        statue.transform.position = _spawnStatue.position;
	        _house = statue.GetComponent<HouseController>();
            _house.Finish = FinishGame;
	        _elk.Initialize();
	        _inGame = true;

	        Cursor.lockState = CursorLockMode.Locked;
	    }

	    public void FinishGame()
	    {
	        if (!_inGame)
	            return;

            _player.GetComponent<PlayerMovement>().IsInControl = false;
            _elk.Stop();
	        _spawner.IsSpawning = false;
	        _spawner.CleanUp();
            _follow.TargetTransform = false;
            _menu.gameObject.SetActive(true);
	        _inGame = false;
            Cursor.lockState = CursorLockMode.None;
            EventScript.Instance.SetSelectedGameObject(GameObject.Find("StartButton"));

        }

        void ReloadGame()
        {
            FinishGame();
            Invoke("StartGame", .05f);
        }

        void ExitGame()
        {
            Application.Quit();
        }
	}
}

