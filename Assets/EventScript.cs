﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Porcupine
{
    public class EventScript : MonoBehaviour
    {
        public static EventSystem Instance;
        private GameObject _storedObj;
        private GameObject _lastSelected;
        private AudioSource _source;

        // Use this for initialization
        void Awake()
        {
            this.CheckIfAlreadyExists(true);
            DontDestroyOnLoad(this);
            Instance = GetComponent<EventSystem>();
            _storedObj = Instance.firstSelectedGameObject;
        }

        // Update is called once per frame
        void Update()
        {
            if (Instance.currentSelectedGameObject != _storedObj)
            {
                if (Instance.currentSelectedGameObject == null)
                {
                    Instance.SetSelectedGameObject(_storedObj);
                }
                {
                    _storedObj = Instance.currentSelectedGameObject;
                }
            }

            if (_lastSelected != Instance.currentSelectedGameObject)
            {
                _lastSelected = Instance.currentSelectedGameObject;
            }
        }
    }
    
}

